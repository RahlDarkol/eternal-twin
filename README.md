# Eternal-Twin 

- [Discord](https://discord.gg/ERc3svy)
- [Wiki](https://wiki.eternalfest.net/)
- [Main repository](https://gitlab.com/eternal-twin/eternal-twin)

The 2020-03-27, [Motion-Twin announced](https://twinoid.com/fr/article-fr/6437/twinoid-et-les-jeux-web-de-motion-twin-evoluent) that they plan to eventually shut down their game websites.

> Nous serons donc amenés à les [nos jeux] fermer progressivement, au fur et à mesure de leur baisse de fréquentation.

As a response, many player decided to create Eternal-Twin. Eternal-Twin is an archival project aiming to keep all the games created by Motion-Twin (see game list below) available, despite the end-of-life of the Flash plugin or eventual closure of their official websites.

Motion-Twin was a major actor in the Flash games ecosystem and had a large presence in France and worldwide. As such, many of their games are still well beloved by player. They are also a nice part of the internet's history and it would be sad to lose those games.

Beyond their games, Motion-Twin also managed to create some strong on-line communities through their forums and a/social network _Twinoid_. We plan to also archive the publicly available messages.

## Game list

Here is the list of games created by Motion-Twin. We would like to archive them all.

- AlphaBounce
- Arkadeo
- Caféjeux
- Corporate Soccer
- CroqueMotel
- Die2Night (Hordes)
- Dinocard
- Dinoparc
- DinoRPG
- Fever
- Fever (Classic)
- Frutiparc
- Hammerfest
- HyperLiner
- Intrusion
- Kingdom
- Miniville
- MonLapin
- Mush
- Naturalchimie
- Naturalchimie (Classic)
- Pioupiouz
- Socratomancie
- Teacher-Story
- Twinoid

(please add any missing game)

## What

The goal is to provide a playable version of the MT game as of their latest version.
We may add some bug fixes but not change any game-play element. If someone wants to add new features, they should be separate.

Regarding the social features, we plan to at the very least provide a way to download an archive: forums, Twinoid nexus messages, private messages.

## Who

Any players wanting to help can join and help.
One team per game and a team to overview shared features and help.

We'll need both developers and people willing to look for information.

## How

Our main target are web browsers.

We'll try to reuse most of the client SWF files and focus on reimplementing the server-side.
The SWF files will either be played through Open Flash or recreated to support HTML.

On the server side, we use the following technologies:
- Node.js
- Typescript
- Postgres (SQL)
- Nginx
- PHP

## What about the administrators?

This is an initiative by the players, it was not approved by the administrators. We did not have any contact with the MT administrators at this moment.

If the project progresses, we plan to ask them to release us the right to maintain the games.

## Monetization

We do not plan to monetize this project, not accept any donations.
The server costs are currently low enough that we can pay for them ourselves.

## Knowledge base

This repository contains all we know about the Motion Twin games. It does not contain any custom code. Just data backups and useful information.

For each game, we try to retrieve all the data available. If you have some files that we don't have, please post them. If you know some help site for a game, please add a link to it. If you know some information about a game (bugs, network protocols, trivia, etc.).
